//
//  ViewController.swift
//  EtiqaAssessment
//
//  Created by Danial Fajar on 28/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fabButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var listData = [ListData]()
    var searchResultData = [ListData]() //for search result
    var manageObjectContext: NSManagedObjectContext? {
        return context
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationView()
        self.setupTableView()
        self.setupFabButton()
        self.setupSearchBar()
        self.noDataLbl.text = "You have not created any list yet.\nPlease tap \"+\" to create a to-do-list now." //setup label text programmatically for translation
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.fetchData { (data) in
            if let data = data{
                self.listData = data
                self.tableView.reloadData()
            }
            //always update UI in main thread// check got data or not
            DispatchQueue.main.async {
                self.setupView()
            }
        }
        //hide search bar
        self.tableView.setContentOffset(CGPoint(x: 0, y: 56), animated: false)
    }
    
    // MARK: - Function
    //setup navigationView
    func setupNavigationView(){
        self.title = "To-Do List" // set title page
        self.navigationController?.navigationBar.barTintColor = UIColor.hexStringToUIColor(hex: "#FEC40E") //set navigation bar color
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black] // set title color
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes // set title color
        
        //setup for switch for darkmode
        let switchView = UISwitch()
        switchView.addTarget(self, action: #selector(self.preferencesToggled(_:)), for: .valueChanged)
        let rightBarButton = UIBarButtonItem(customView: switchView)
        self.navigationController?.navigationBar.topItem?.setRightBarButton(rightBarButton, animated: false)
               
        // set & check dark mode according to user preferences
        let darkMode = UserDefaults.standard.bool(forKey: "darkMode")
        overrideUserInterfaceStyle = darkMode ? .dark : .light
        switchView.isOn = darkMode ? true : false
    }
    
    func setupView(){
        //if no data, show label no data, else show table
        self.noDataView.isHidden = self.listData.count == 0 ? false : true
        self.tableView.isHidden = self.listData.count == 0 ? true : false
    }
    
    func setupTableView() {
        //set tableview delegate
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //set Uiview to tableview footer to hide extra line
        self.tableView.tableFooterView = UIView()
        //register nib cell
        self.tableView.register(UINib(nibName: "HomePageCell", bundle: nil), forCellReuseIdentifier: "HomePageCell")
    }
    
    func setupFabButton(){
        self.fabButton.addTarget(self, action: #selector(goToAddPage(_:)), for: .touchUpInside)
        
        //set shadow for fab button
        self.fabButton.layer.shadowColor = UIColor.black.cgColor
        self.fabButton.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.fabButton.layer.shadowRadius = 4.0
        self.fabButton.layer.shadowOpacity = 0.5
    }
    
    func setupSearchBar(){
        //set searchbar delegate
        self.searchBar.delegate = self
    }

    // MARK: - Function Get Data
    func fetchData(completion: @escaping([ListData]?) -> Void) {
        manageObjectContext?.perform {
            
            let request: NSFetchRequest<ListData> = ListData.fetchRequest()
            
            do {
                let data = try self.manageObjectContext?.fetch(request)
                completion(data)
            }
            catch{
                print("Could not fetch data from CoreData:\(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - Event Actions
    @objc func goToAddPage(_ sender: UIButton){
        let nextPage = UIStoryboard(name: "SecondPage", bundle: nil).instantiateViewController(withIdentifier: "SecondPage") as! SecondViewController
        self.navigationController?.pushViewController(nextPage, animated: true)
    }
    
    //toggle action for darkmode
    @objc func preferencesToggled(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn ? true : false, forKey: "darkMode")
        overrideUserInterfaceStyle = sender.isOn ? .dark : .light
    }
}

// MARK: - Tableview data source , tableview delegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResultData.count > 0 ? self.searchResultData.count : self.listData.count //if not searching, return all data.. else, search data
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCell", for: indexPath) as! HomePageTableViewCell
        let data = self.searchResultData.count > 0 ? self.searchResultData[indexPath.row] : self.listData[indexPath.row] //if not searching, load all data.. else, search data
        cell.configureCell(data: data)
        cell.delegate = self
        cell.checkBoxButton.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextPage = UIStoryboard(name: "SecondPage", bundle: nil).instantiateViewController(withIdentifier: "SecondPage") as! SecondViewController
        let data = self.searchResultData.count > 0 ? self.searchResultData[indexPath.row] : self.listData[indexPath.row] //if not searching, use all data.. else, use search data
        nextPage.listData = data
        nextPage.isEdit = true
        self.navigationController?.pushViewController(nextPage, animated: true)
    }
    
    @available(iOS 11.0, *)
    //Remove data by swipping the table cell. For IOS 11 and above.. if ios below need to use editingStyle tableview delegate instead
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action =  UIContextualAction(style: .normal, title: nil, handler: { (action,view,completionHandler ) in
            //do stuff
            let data = self.listData[indexPath.row]
            
            context?.delete(data)

            appDelegate?.saveContext{ (status) in
                if status {
                    do {
                        self.listData = try context?.fetch(ListData.fetchRequest()) ?? []
                        tableView.deleteRows(at: [indexPath], with: .fade)
                        DispatchQueue.main.async {
                            self.setupView() //check again view
                        }
                    } catch {
                       print("Failed to fetch data.")
                    }
                } else {
                    self.alertPopup("Failed to update existing listing after delete.")
                }
            }
            
            tableView.reloadData()
            
            completionHandler(true)
        })
        action.image = UIImage(named: "ic-action-delete.png")
        action.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [action])
        
        return configuration
    }
}

// MARK: - UISearchBarDelegate delegate
extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, delay: 0.15, options: .curveEaseIn, animations: {
                 self.searchBar.showsCancelButton = true //show cancel button
            }, completion: { finished in
            })
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchResultData.removeAll() // clear array
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet(integer: 0), with: .fade)
                self.tableView.setContentOffset(.zero, animated: true)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, delay: 0.15, options: .curveEaseOut, animations: {
                 self.searchBar.showsCancelButton = false //hide cancel button
            }, completion: { finished in
            })
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true) //dismiss keyboard
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchResultData.removeAll() // clear array first
        self.searchResultData = self.listData.filter({($0.title?.contains(searchBar.text ?? "") ?? false)}) //filter data
        if self.searchResultData.count > 0 {
            DispatchQueue.main.async {
              self.tableView.reloadSections(IndexSet(integer: 0), with: .fade) //reload section
              self.tableView.setContentOffset(.zero, animated: true)
            }
        }else{
            self.alertPopup("Sorry, couldn't find any results matching \(searchBar.text ?? ""). Please try again.")
        }
    }
}

// MARK: - HomePageTableViewCellDelegate delegate
extension ViewController: HomePageTableViewCellDelegate {
    func doCheckBox(index: Int) {
        let listObject = self.listData[index]
        //set value complete to true/false
        listObject.setValue(!listObject.isComplete, forKey: "isComplete")
        
        appDelegate?.saveContext{ (status) in
            if status {
                //get indexPath of the data to refresh its row only
                let indexPath = IndexPath(item: index, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            } else {
                self.alertPopup("Failed to update existing listing.")
            }
        }
    }
}

