//
//  SecondViewController.swift
//  EtiqaAssessment
//
//  Created by Danial Fajar on 29/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var startDateTextField: UITextField!
    
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var endDateTextField: UITextField!
    
    @IBOutlet weak var actionBtn: UIButton!
        
    var isTextViewActive: Bool = false //for keyboard purposes
    var isEdit: Bool = false //check new data or edit data.. default value is false
    var listData: ListData?
        
    var manageObjectContext: NSManagedObjectContext? {
        return context
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //setup view
        self.setupView()
        //setup textfield & textview
        self.setupTextField()
        //setup button
        self.setupButton()
            
        //Others
        //setup notification for date picker
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            
        //tap gesture to close keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard(_:)))
        self.view.addGestureRecognizer(tap)
    }
        
    //clear notification instant when this page is popped
    deinit {
        Foundation.NotificationCenter.default.removeObserver(self)
    }
        
    // MARK: - Function
    func setupButton(){
        //set button title based on mode
        self.actionBtn.setTitle( isEdit ? "Save Now" : "Create Now", for: .normal)
        //set button target
        self.actionBtn.addTarget(self, action: #selector(doAction(_:)), for: .touchUpInside)
    }
        
    func setupView(){
        self.determineDarkMode()
        
        if isEdit { //if edit mode, assign value to input field
            self.textView.text = self.listData?.title
            self.startDateTextField.text = self.listData?.startDate
            self.endDateTextField.text = self.listData?.endDate
        }
    }
    
    func setupTextField(){
        //setup textfield and textview delegate
        self.startDateTextField.delegate = self
        self.endDateTextField.delegate = self
        self.textView.delegate = self
        
        //set textview border & border color
        self.textView.layer.borderColor = UIColor(named: "BlackDarkMode")?.resolvedColor(with: self.traitCollection).cgColor
        self.textView.layer.borderWidth = 1
    }
    
    //set datepicker to textfield
    func datePicker(_ textField: UITextField) {
          if textField == self.startDateTextField {
              let startDatePicker = UIDatePicker(frame:CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.size.width, height: 200)))
              startDatePicker.datePickerMode = .date
              startDatePicker.minimumDate = Date()  //set the current date as a minimum.. if edit mode, then minimum is the existing value
              startDatePicker.date = isEdit ? self.startDateTextField.text!.changeStringToDate() : Date()
              textField.inputView = startDatePicker
                  
              startDatePicker.addTarget(self, action: #selector(pickStartDate(_:)), for: .valueChanged)
                  
              // set toolBar
              let toolBar = UIToolbar().toolBarPicker(mySelect: #selector(closeDatePicker(_:)), tag: 1)
              textField.inputAccessoryView = toolBar
                  
          } else if textField == self.endDateTextField {
              if self.startDateTextField.text == "" {
                  self.alertPopup("Please select start date.")
              }else {
                  let startDatePicker = UIDatePicker(frame:CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.size.width, height: 200)))
                  startDatePicker.datePickerMode = .date
                  startDatePicker.minimumDate = self.getNextDay(stringDate: self.startDateTextField.text!.changeStringToDate()) //set the next day of start date as minimum end date.. if edit mode, then minimum is the existing value
                  startDatePicker.date = isEdit ? self.endDateTextField.text!.changeStringToDate() : self.getNextDay(stringDate: self.startDateTextField.text!.changeStringToDate())
                  textField.inputView = startDatePicker
                      
                  startDatePicker.addTarget(self, action: #selector(pickEndDate(_:)), for: .valueChanged)
                      
                  // set toolBar
                  let toolBar = UIToolbar().toolBarPicker(mySelect: #selector(closeDatePicker(_:)), tag: 2)
                  textField.inputAccessoryView = toolBar
              }
          }
      }
          
    //get date of next day based on date given
    //parameter date
    //return value date
    func getNextDay(stringDate: Date) -> Date {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: stringDate) ?? Date() //if empty, send current date
        return date
    }
        
    // MARK: - Function Save Data
    //save to core Data
    func saveToCoreData(completion: @escaping (Bool)-> Void){
        manageObjectContext?.perform {
            do {
                try self.manageObjectContext?.save()
                completion(true)
            } catch let error{
                completion(false)
                #if DEBUG
                print( "Couldnt save data to core data: \(error.localizedDescription)")
                #endif
            }
        }
    }
        
    // MARK: - Event Actions
    @objc func closeKeyboard(_ sender: UITapGestureRecognizer){
        if self.isTextViewActive { //only close for textview
            self.view.endEditing(true)
        }
    }
      
    //close date picker by press done button
    @objc func closeDatePicker(_ sender: UIButton){
        if sender.tag == 1 {
            //if value empty and user click done, update start date textfield with current date
            self.startDateTextField.text = self.startDateTextField.text!.isEmpty ? Date().dateToString() : self.startDateTextField.text
        }else{
            //if value empty and user click done, update end date textfield with next day based on start date data
            self.endDateTextField.text = self.endDateTextField.text!.isEmpty ? self.getNextDay(stringDate: self.startDateTextField.text!.changeStringToDate()).dateToString() : self.endDateTextField.text
        }
        self.view.endEditing(true)
    }
    
    //submit data
    @objc func doAction(_ sender: UIButton){
        if self.textView.text.isEmpty || self.textView.text == "Please key in your To-Do title here.." {
            self.alertPopup("Please enter to-do title.")
        }else if self.startDateTextField.text!.isEmpty {
            self.alertPopup("Please enter start date.")
        }else if self.endDateTextField.text!.isEmpty {
            self.alertPopup("Please enter end date.")
        }else if self.endDateTextField.text!.elementsEqual(self.startDateTextField.text!) {
            self.alertPopup("End date and start date cannot be same.")
        }else if self.startDateTextField.text!.changeStringToDate() > self.endDateTextField.text!.changeStringToDate(){
            self.alertPopup("Start date cannot larger than end date.")
        }else {
            //if in edit mode then update data
            if isEdit {
                guard let listObject = self.listData else { return }
                    
                listObject.setValue(self.textView.text, forKey: "title")
                listObject.setValue(self.startDateTextField.text, forKey: "startDate")
                listObject.setValue(self.endDateTextField.text, forKey: "endDate")
                listObject.setValue(self.listData?.isComplete, forKey: "isComplete")
                    
                appDelegate?.saveContext{ (status) in
                    if status {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.alertPopup("Failed to update existing listing.")
                    }
                }
            } else { //else save data
                if let moc = manageObjectContext {
                    let listDataObject = ListData(context: moc)
                        
                        
                    listDataObject.title = self.textView.text
                    listDataObject.startDate = self.startDateTextField.text
                    listDataObject.endDate = self.endDateTextField.text
                    listDataObject.isComplete = false
                        
                    self.saveToCoreData{ (status) in
                        if status {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.alertPopup("Failed to save the data.")
                        }
                    }
                }
            }
        }
    }
        
    //detect keyboard is show or not, if show and not textview, update UI
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve: UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        let keyboardFrame = keyboardSize.cgRectValue
            
        if self.isTextViewActive == false {
            if self.view.frame.origin.y == 0 {
                //update UI in main thread
                DispatchQueue.main.async {
                    UIView.animate(withDuration: duration, delay: 0, options: animationCurve,
                    animations: { self.view.frame.origin.y -= keyboardFrame.height },
                    completion: { (_) in
                        self.view.layoutIfNeeded()
                    })
                }
                
            }
        }
    }
     
    //hide keyboard
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            DispatchQueue.main.async {
                self.view.frame.origin.y = 0
            }
        }
    }
    
    //pick start date
    @objc func pickStartDate(_ sender: UIDatePicker){
        self.startDateTextField.text = sender.date.dateToString()
    }
    
    //pick end date
    @objc func pickEndDate(_ sender: UIDatePicker){
        self.endDateTextField.text = sender.date.dateToString()
    }
}

// MARK: - TextField Delegate
extension SecondViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //For picker
        if textField == self.startDateTextField {
            self.datePicker(textField)
        } else if textField == self.endDateTextField {
            self.datePicker(textField)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       //For picker
       if textField == self.endDateTextField {
           if self.startDateTextField.text == "" {
               self.endDateTextField.resignFirstResponder()
               
           }
       }
    }
}

// MARK: - TextView Delegate
extension SecondViewController: UITextViewDelegate {
 
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.isTextViewActive = true
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        //if not placeholder, dont clear text
        //if not placeholder, change text color to white/black
        textView.text = textView.text.elementsEqual("Please key in your To-Do title here..") ? "" : textView.text
        textView.textColor = UIColor(named: "BlackDarkMode")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.isTextViewActive = false
        //if empty, change text color to dark gray (placeholder)
        //if empty, put back placeholder
        textView.textColor = textView.text.isEmpty ? UIColor.darkGray : UIColor(named: "BlackDarkMode")
        textView.text = textView.text.isEmpty ? "Please key in your To-Do title here.." : textView.text
    }
    
}
