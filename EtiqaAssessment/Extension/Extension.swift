//
//  Extension.swift
//  EtiqaAssessment
//
//  Created by Danial Fajar on 28/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIViewController
extension UIViewController {
    //determine darkmode based on user preferences
    func determineDarkMode() {
        let darkMode = UserDefaults.standard.bool(forKey: "darkMode")
        overrideUserInterfaceStyle = darkMode ? .dark : .light
    }
    
    //prompt alert error
    //parameter string message
    func alertPopup(_ message: String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - UIToolbar
extension UIToolbar {
    //set toolbar for datepicker
    func toolBarPicker(mySelect : Selector, tag: Int) -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: mySelect)
        doneButton.tag = tag
        doneButton.tintColor = UIColor.hexStringToUIColor(hex: "#FEC40E")
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.layoutIfNeeded()
        
        return toolBar
    }
}

// MARK: - UIColor
extension UIColor {
    //function for change hex string to UIColor
    class func hexStringToUIColor (hex:String) -> UIColor {
           var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

           if (cString.hasPrefix("#")) {
               cString.remove(at: cString.startIndex)
           }

           if ((cString.count) != 6) {
               return UIColor.gray
           }

           var rgbValue:UInt64 = 0
           Scanner(string: cString).scanHexInt64(&rgbValue)

           return UIColor(
               red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
               green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
               blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
               alpha: CGFloat(1.0)
           )
       }
}

// MARK: - String
extension String {
    //change string date to date format
    func changeStringToDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.date(from: self) ?? Date() // if nill return current date
    }
}

// MARK: - Date
extension Date {
    //change from format date to string
    //parameter date
    //return value string
    func dateToString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter.string(from: self)
    }
}
