//
//  HomePageTableViewCell.swift
//  EtiqaAssessment
//
//  Created by Danial Fajar on 28/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit

protocol HomePageTableViewCellDelegate {
    func doCheckBox(index: Int) //delegate for tick checkbox
}

class HomePageTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var timeLeftLbl: UILabel!
    
    @IBOutlet weak var startDateValueLbl: UILabel!
    @IBOutlet weak var endDateValueLbl: UILabel!
    @IBOutlet weak var timeLeftValueLbl: UILabel!
    
    @IBOutlet weak var statusTitleLbl: UILabel!
    @IBOutlet weak var statusValueLbl: UILabel!
    
    @IBOutlet weak var tickLbl: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    var delegate: HomePageTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //Setup View
        self.setupView()
        self.setupLabel()
        self.setupButton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //setup view
    func setupView(){
        //add shadow effect
        self.shadowView.layer.shadowColor = UIColor.black.cgColor
        self.shadowView.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.shadowView.layer.shadowRadius = 4.0
        self.shadowView.layer.shadowOpacity = 0.5
        self.shadowView.layer.cornerRadius = 10.0
        self.cellContentView.layer.cornerRadius = 10.0
    
    }
    
    //setup label text programmatically, easy for translation later on
    func setupLabel(){
        self.startDateLbl.text = "Start Date"
        self.endDateLbl.text = "End Date"
        self.timeLeftLbl.text = "Time Left"
        self.statusTitleLbl.text = "Status"
        self.tickLbl.text = "Tick if completed"
    }
    
    //setup button
    func setupButton(){
        self.checkBoxButton.addTarget(self, action: #selector(doCheckBox(_:)), for: .touchUpInside)
    }

    //configure cell data
    func configureCell(data: ListData){
        //variable for date thing
        let startDate = data.startDate ?? ""
        let endDate = data.endDate ?? ""
        let todayDate = Date().dateToString()
        
        self.titleLbl.text = data.title?.capitalized
        
        self.startDateValueLbl.text = data.startDate
        
        self.endDateValueLbl.text = data.endDate
        
        //need to change back to date data type to compare if not will get error result.
        //compare string start date with current date and end date with current date.
        self.timeLeftValueLbl.text = endDate.changeStringToDate() > todayDate.changeStringToDate() ? (startDate.changeStringToDate() <= todayDate.changeStringToDate() ? self.getCountdown(endDate) : "Haven't started yet" ) : "Expired"
        
        self.statusValueLbl.text = data.isComplete ? "Completed" : "Incomplete"
        
        self.checkBoxImage.image = data.isComplete ? UIImage(named:"ic-checkbox-tick.png") : UIImage(named: "ic-checkbox-untick.png")
    }
    
    //get countdown
    //parameter end date
    func getCountdown(_ endDate: String) -> String{
        let calendar = Calendar.current
        //set start date using today date
        let startDatecomponents = calendar.dateComponents([.hour, .minute, .month, .year, .day], from: Date())

        guard let start = calendar.date(from: startDatecomponents) else { return "" }


        //set the due date based on due date data.
        let endDatecomponents = calendar.dateComponents([.hour, .minute, .month, .year, .day], from: endDate.changeStringToDate())
        guard let end = calendar.date(from: endDatecomponents) else { return "" }
        
        //change the seconds to hours, minutes and days
        let CompetitionDayDifference = calendar.dateComponents([.hour, .minute], from: start, to: end)


        //set value to variable
        let hoursLeft = CompetitionDayDifference.hour ?? 0
        let minutesLeft = CompetitionDayDifference.minute ?? 0
        
        
        
        let string = "\(String(describing: hoursLeft)) hrs \(String(describing: minutesLeft)) min"
        
        return string
    }
    
    // MARK: - Event Actions
    //action for tick checkbox
    @objc func doCheckBox(_ sender: UIButton){
        self.delegate?.doCheckBox(index: sender.tag)
    }
}
